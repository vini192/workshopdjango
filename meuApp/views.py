from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from .forms import PessoaForm
from .models import Pessoa

# Create your views here.

def pessoaList(request):
    pessoas = Pessoa.objects.all()
    return render(request, 'pessoaList.html', {"pessoas": pessoas})

def pessoaCreate(request):
    form = PessoaForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('pessoaList')

    return render(request, 'pessoaCreate.html', {'form': form})

def pessoaUpdate(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if form.is_valid():
        form.save()
        return redirect('pessoaList')
    return render(request, 'pessoaCreate.html', {'form': form})

def pessoaDelete(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    form = PessoaForm(request.POST or None, instance=pessoa)
    if request.method == 'POST':
        pessoa.delete()
        return redirect('pessoaList')

    return render(request, 'pessoaDeletec.html', {'form':form})
