from django.db import models
# Create your models here.

class Pessoa(models.Model):
    nome = models.CharField(max_length=10)
    cpf = models.CharField(max_length=11)
    SEX_CHOI = {
        ('U', 'UNSURE',),
        ('F', 'FEMALE',),
        ('M', 'MALE',),
    }
    sexo = models.CharField(max_length=1, choices=SEX_CHOI, null=True, )
    def __str__(self):
        return self.nome
