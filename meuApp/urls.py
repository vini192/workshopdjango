from django.urls import path
from meuApp import views

urlpatterns = [
    path('list/', views.pessoaList, name='pessoaList'),
    path('create/', views.pessoaCreate, name='pessoaCreate'),
    path('update/<int:id>', views.pessoaUpdate, name='pessoaUpdate'),
    path('delete/<int:id>', views.pessoaDelete, name='pessoaDelete'),
]